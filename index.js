const Event = require('./modelo/event');
const Usuario = require('./modelo/user');
const AuthUser = require('./modelo/authUser');
const http = require('http');
const https = require('https');
const request = require('request');
const fs = require('fs');



//inicializar var. global resultado
let resultado = {};

//funcion de guardar imagen
let guardarImagen = (url, name) => {
    https.request(url)
        .on('response', function (response) {
            let body = ''
            response.setEncoding('binary');
            response.on('data', function (chunk) {
                body += chunk
            }).on('end', function () {
                fs.writeFileSync(
                    './img/users/' + name + ".jpg ", body, 'binary');
            });
        })
        .end();
}


let atenderPeticion = (request, response) => {

    //validando token
    let token = request.headers['authorization'];
    let valToken = AuthUser.validarToken(token);

    //inicializar var. global login
    let login = {};

    //cogemos del login lo que nos interesa : el id y el email.
    if (valToken) {
        login = {
            id: valToken.id,
            email: valToken.email
        };
    }

    //Por el tema de las cors
    response.setHeader('Content-Type', 'application/json');
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Request-Method', '*');
    response.setHeader("Access-Control-Allow-Methods", "GET, POST,PUT, DELETE, OPTIONS");
    response.setHeader('Access-Control-Allow-Headers', '*');
    if (request.method === "OPTIONS") { /** CORS Fix */
        response.end();
        return;
    }

    // fallo en mostrar imagenes si le pongo esta cabecera
    // response.writeHead(
    //     200, {
    //         "Content-Type": "application/json"
    //     }
    // );

    //Switch con los 4 posibles casos: GET, POST, PUT, DELETE
    switch (request.method) {
        case 'GET':
            if (request.url.startsWith('/img/events/') || request.url.startsWith('/img/users/')) {
                if (fs.existsSync('.' + request.url)) {
                    response.setHeader(
                        "Content-Type", "image/jpg"
                    );

                    fs.readFile('.' + request.url, (error, data) => {
                        response.end(data);
                    });
                } else {
                    response.end();
                }

            }
            //2.3 Validación del token -> probado y funcionando
            if (request.url === '/auth/token') {
                if (valToken)
                    resultado = {
                        ok: true,
                        error: false,
                    };
                else
                    resultado = {
                        ok: false,
                        error: true
                    };
                response.end(JSON.stringify(resultado));
            }

            // Conexion a google
            if (request.url === '/auth/google') {
                response.writeHead(
                    200, {
                        "Content-Type": "application/json"
                    }
                );
                let token = request.headers['authorization'];
                https.request('https://www.googleapis.com/plus/v1/people/me?access_token=' + token)
                    .on('response', function (respons) {
                        let body = ''
                        respons.on('data', function (chunk) {
                            body += chunk
                        }).on('end', function () {
                            let datos = JSON.parse(body);

                            Usuario.crearUsuarioGoogle(datos).then(respuesta => {
                                resultado = {
                                    ok: true,
                                    token: respuesta,
                                }
                                response.end(JSON.stringify(resultado));
                            }).catch(error => {
                                resultado = {
                                    ok: false,
                                    errorMessage: error
                                }
                                response.end(JSON.stringify(resultado));
                            });
                            guardarImagen(datos.image.url, datos.id);
                        });
                    })
                    .end();
            }
            //conexion a facebook
            else if (request.url === '/auth/facebook') {
                response.writeHead(
                    200, {
                        "Content-Type": "application/json"
                    }
                );
                let token = request.headers['authorization'];
                https.request('https://graph.facebook.com/v2.11/me?fields=id,name,email,picture&access_token=' + token)
                    .on('response', function (respons) {
                        let body = ''
                        respons.on('data', function (chunk) {
                            body += chunk
                        }).on('end', function () {
                            let data = JSON.parse(body);
                            guardarImagen(data.picture.data.url, data.id);

                            Usuario.crearUsuarioFacebook(data).then(respuesta => {
                                resultado = {
                                    ok: true,
                                    token: respuesta,
                                }
                                response.end(JSON.stringify(resultado));
                            }).catch(error => {
                                resultado = {
                                    ok: false,
                                    errorMessage: error
                                }
                                response.end(JSON.stringify(resultado));
                            });


                        });
                    })
                    .end();
            }
            //4.1.3 Usuarios que acuden a un evento
            else if (request.url.startsWith('/events/attend/')) {
                //Se parte la URL y se obtiene la id
                let partes = request.url.split('/');
                let id = partes[3];
                Usuario.listarUsuariosAsisten(id).then(respuesta => {
                    token = AuthUser.generarToken(login);

                    resultado = {
                        ok: true,
                        error: false,
                        result: respuesta,
                        token: token
                    };
                    response.end(JSON.stringify(resultado));

                }).catch(error => {
                    resultado = {
                        ok: false,
                        error: true,
                        errorMessge: 'Error: ' + error
                    };
                    response.end(JSON.stringify(resultado));
                });
            }
            //--------- EVENTOS -----------------
            else if (valToken) {
                //3.2.1 Listado de todos los eventos -> probado y funciona
                if (request.url === '/events') {
                    Event.listarEventos(login).then(respuesta => {
                        token = AuthUser.generarToken(login);

                        resultado = {
                            error: false,
                            ok: true,
                            events: respuesta,
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            ok: false,
                            error: true,
                            errorMessage: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));
                    });
                }
                //3.2.2 Listado de todos los eventos del usuario actual -> probado y funciona
                else if (request.url === '/events/mine') {

                    Event.listarMisEventos(login).then(respuesta => {
                        token = AuthUser.generarToken(login);

                        resultado = {
                            ok: true,
                            error: false,
                            events: respuesta,
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            error: true,
                            ok: false,
                            errorMessge: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));
                    });

                }
                //3.2.3 Listado de todos los eventos a los que asiste el usuario actual -> probado y funciona
                else if (request.url === '/events/attend') {

                    Event.listarEventosAsistir(login).then(respuesta => {
                        token = AuthUser.generarToken(login);

                        resultado = {
                            error: false,
                            ok: true,
                            events: respuesta,
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            error: true,
                            errorMessge: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));
                    });

                }
                //3.2.4 Datos de un evento en concreto (buscar por id) -> probado funciona
                else if (request.url.startsWith('/events/')) {

                    //Se parte la URL y se obtiene la id
                    let partes = request.url.split('/');
                    let idEvent = partes[2];

                    Event.buscarEventoPorId(idEvent, login).then(respuesta => {
                        token = AuthUser.generarToken(login);

                        resultado = {
                            ok: true,
                            error: false,
                            event: respuesta[0],
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            ok: false,
                            error: true,
                            errorMessage: error
                        };
                        response.end(JSON.stringify(resultado));
                    });
                }

                //--------- USUARIOS -----------------//

                //4.1.1 Ficha del usuario actual -> probado y funciona
                else if (request.url === '/profile/me' || request.url === '/profile') {

                    Usuario.buscarUsuarioLogueado(login.id).then(respuesta => {
                        token = AuthUser.generarToken(login);
                        resultado = {
                            error: false,
                            user: respuesta,
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            error: true,
                            errorMessge: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));
                    });
                }
                //4.1.2 Ficha de un usuario concreto -> probado y funciona
                else if (request.url.startsWith('/users/') || request.url.startsWith('/profile/')) {

                    //Se parte la URL y se obtiene la id
                    let partes = request.url.split('/');
                    let id = partes[2];
                    //parseamos a int para que sea number, si se introduce otro caracter que no sea numérico quedaria como NaN y dará el error como true.
                    id = parseInt(id);
                    //se comprueba que sea un numero y no un NaN
                    if (typeof (id) === 'number') {
                        //creamos un objeto para pasarlo al método ya que recibe un objeto
                        Usuario.buscarUsuarioPorId(id, login.id).then(respuesta => {
                            token = AuthUser.generarToken(login);

                            if (id === login.id) {
                                respuesta.mine = true;
                            } else
                                respuesta.mine = false;
                            resultado = {
                                ok: true,
                                error: false,
                                user: respuesta,
                                token: token
                            };
                            response.end(JSON.stringify(resultado));

                        }).catch(error => {
                            resultado = {
                                ok: false,
                                error: true,
                                errorMessge: 'Error: ' + error
                            };
                            response.end(JSON.stringify(resultado));
                        });
                    }
                }
                //4.1.3 Usuarios que acuden a un evento
                else if (request.url.startsWith('/users/event/')) {


                    //Se parte la URL y se obtiene la id
                    let partes = request.url.split('/');
                    let id = partes[3];
                    Usuario.listarUsuariosAsisten(id).then(respuesta => {
                        token = AuthUser.generarToken(login);

                        resultado = {
                            ok: true,
                            error: false,
                            result: respuesta,
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            ok: false,
                            error: true,
                            errorMessge: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));
                    });
                }

            }


            // } else {
            //     //Si no se esta logueado se enviara un 403 a traves de la cabecera
            //     response.writeHead(
            //         403, {
            //             "Content-Type": "application/json"
            //         }
            //     );

            //     resultado = {
            //         error: true,
            //         errorMessge: 'User is not logged.'
            //     };

            //     response.end(JSON.stringify(resultado));


            break;

        case 'POST':

            //------------- LOGIN ----------------//

            //2.1 Registro de Usuarios -> probado y funcionando
            if (request.url === '/auth/register') {
                response.writeHead(
                    200, {
                        "Content-Type": "application/json"
                    }
                );
                let body = [];

                request.on('data', (chunk) => {
                    body.push(chunk);
                }).on('end', () => {
                    body = Buffer.concat(body).toString();
                    let data = JSON.parse(body);
                    let avatar = data.avatar;
                    avatar = avatar.replace(/^data:image\/jpg;base64,/, "");
                    avatar = avatar.replace(/^data:image\/png;base64,/, "");
                    avatar = avatar.replace(/^data:image\/jpeg;base64,/, "");
                    avatar = Buffer.from(avatar, 'base64');

                    let date = new Date();
                    let nameFile = date.getDate() + date.getTime() + date.getMilliseconds() + '.jpg';
                    fs.writeFileSync('./img/users/' + nameFile, avatar);
                    data.avatar = nameFile;
                    let newUser = new Usuario(data);

                    newUser.crearUsuario().then(respuesta => {
                        //Se crea un token para el nuevo usuario
                        token = AuthUser.generarToken({
                            id: respuesta,
                            email: newUser.email
                        });

                        resultado = {
                            error: false,
                            ok: true,
                            result: respuesta,
                            token: token
                        };

                        response.end(JSON.stringify(resultado));

                    });

                });

            }
            // 2.2 Autenticación local de usuarios -> probado y funcionando
            else if (request.url === '/auth/login') {
                response.writeHead(
                    200, {
                        "Content-Type": "application/json"
                    }
                );
                let body = [];
                request.on('data', (chunk) => {
                    body.push(chunk);
                }).on('end', () => {
                    body = Buffer.concat(body).toString();
                    let datos = JSON.parse(body);
                    let coordUser = new Usuario(datos);

                    AuthUser.authUser(datos).then(respuesta => {
                        token = respuesta;
                        resultado = {
                            error: false,
                            token: token[0],
                            ok: true,
                        };

                        let idToken = AuthUser.validarToken(token[0]).id;

                        // actualizamos en la base de datos las coordenadas con la geolocalizacion que nos da el cliente, asi cada vez que haga login tendra una localizacion diferente para poder ver la distancia de los eventos desde donde se encuentra.
                        coordUser.actualizarCoordenadas(idToken, datos.lat, datos.lng).then(respuest => {

                            resultado = {
                                ok: true,
                                error: false,
                                result: respuest,
                            };
                        })
                        response.end(JSON.stringify(resultado));
                    }).catch(error => {
                        resultado = {
                            error: true,
                            ok: false,
                            errorMessge: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));
                    });
                });
            }
            //si existe el token
            else if (valToken) {

                //--------- EVENTOS -----------------//

                //3.3.1 Creación de eventos -> probado funciona
                if (request.url === '/events') {
                    response.writeHead(
                        200, {
                            "Content-Type": "application/json"
                        }
                    );

                    //pasamos un JSON
                    let body = [];
                    request.on('data', (chunk) => {
                        body.push(chunk);
                    }).on('end', () => {
                        body = Buffer.concat(body).toString();
                        let datos = JSON.parse(body);

                        let image = datos.image;
                        image = image.replace(/^data:image\/jpg;base64,/, "");
                        image = image.replace(/^data:image\/png;base64,/, "");
                        image = image.replace(/^data:image\/jpeg;base64,/, "");
                        image = Buffer.from(image, 'base64');

                        let date = new Date();
                        let nameFile = date.getDate() + date.getTime() + date.getMilliseconds() + '.jpg';
                        fs.writeFileSync('./img/events/' + nameFile, image);
                        datos.image = nameFile;
                        let nuevoEvento = new Event(datos);
                        nuevoEvento.creator.id = login.id;
                        nuevoEvento.crearEvento().then(respuesta => {
                            token = AuthUser.generarToken(login);
                            resultado = {
                                error: false,
                                ok: true,
                                events: "id del evento: " + respuesta,
                                token: token
                            };
                            response.end(JSON.stringify(resultado));

                        }).catch(error => {
                            resultado = {
                                error: true,
                                ok: false,
                                errorMsg: 'Error: ' + error
                            };
                            response.end(JSON.stringify(resultado));
                            fs.unlink('./img/events/' + nameFile, () => {});
                        });
                    });

                }
                //3.3.2 Asistencia a eventos ->probado funciona
                else if (request.url.startsWith('/events/attend/')) {

                    //Se parte la URL y se obtiene la id
                    let partes = request.url.split('/');
                    let id = partes[3];
                    id = parseInt(id);
                    //se comprueba que sea un numero y no un NaN
                    if (typeof (id) === 'number') {
                        //pasamos un JSON
                        let body = [];
                        request.on('data', (chunk) => {
                            body.push(chunk);
                        }).on('end', () => {
                            body = Buffer.concat(body).toString();
                            let datos = JSON.parse(body);

                            Event.asistirEvento(id, login.id, datos.tickets).then(respuesta => {
                                token = AuthUser.generarToken(login);
                                resultado = {
                                    ok: true,
                                    error: false,
                                    events: respuesta,
                                    result: 'el número de entradas compradas es: ' + datos.tickets,
                                    token: token
                                };
                                response.end(JSON.stringify(resultado));

                            }).catch(error => {
                                resultado = {
                                    error: true,
                                    errorMessge: 'Error: ' + error
                                };
                                response.end(JSON.stringify(resultado));
                            });;
                        });
                    }
                }
            }
            //Si no se esta logueado se enviara un 403 a traves de la cabecera
            else {
                response.writeHead(
                    403, {
                        "Content-Type": "application/json"
                    }
                );

                resultado = {
                    error: true,
                    errorMessge: 'User is not logged.'
                };

                response.end(JSON.stringify(resultado));
            }
            break;

        case 'PUT':

            //--------- EVENTOS -----------------//

            //3.3.3 Modificación de eventos -> probado y funciona
            //si existe el token
            if (valToken) {
                if (request.url.startsWith('/events/edit/')) {
                    //Se parte la URL y se obtiene la id
                    let partes = request.url.split('/');
                    let id = partes[3];
                    console.log('partes:', partes);
                    id = parseInt(id);
                    //se comprueba que sea un numero y no un NaN
                    if (typeof (id) === 'number') {
                        let body = [];
                        request.on('data', (chunk) => {
                            body.push(chunk);
                        }).on('end', () => {
                            body = Buffer.concat(body).toString();
                            let datos = JSON.parse(body);

                            console.log('datos:', datos);
                            let image = datos.image;
                            image = image.replace(/^data:image\/png;base64,/, "");
                            image = image.replace(/^data:image\/jpg;base64,/, "");
                            image = image.replace(/^data:image\/jpeg;base64,/, "");
                            image = Buffer.from(image, 'base64');

                            let date = new Date();
                            let nameFile = date.getDate() + date.getTime() + date.getMilliseconds() + '.jpg';
                            fs.writeFileSync('./img/events/' + nameFile, image);
                            datos.image = nameFile;

                            let modEvento = new Event(datos);
                            modEvento.creator = login.id;

                            modEvento.actualizarEvento(id, login.id).then(respuesta => {
                                token = AuthUser.generarToken(login);
                                resultado = {
                                    ok: true,
                                    error: false,
                                    event: respuesta,
                                    token: token
                                };
                                response.end(JSON.stringify(resultado));

                            }).catch(error => {
                                resultado = {
                                    ok: false,
                                    error: true,
                                    errorMessge: 'Error: ' + error
                                };
                                response.end(JSON.stringify(resultado));
                            });
                        });
                    }
                }
                //--------- USUARIOS -----------------//

                //4.2 Modificaciones
                else if (request.url === '/users/me' || request.url === '/profile/me') {

                    let body = [];
                    request.on('data', (chunk) => {
                        body.push(chunk);
                    }).on('end', () => {
                        body = Buffer.concat(body).toString();
                        let datos = JSON.parse(body);
                        let modUser = new Usuario(datos);
                        modUser.actualizarEmail_nombre(login.id, modUser).then(respuesta => {
                            token = AuthUser.generarToken(login);

                            resultado = {
                                error: false,
                                user: respuesta,
                                token: token
                            };
                            response.end(JSON.stringify(resultado));

                        }).catch(error => {
                            resultado = {
                                error: true,
                                errorMessge: 'Error: ' + error
                            };
                            response.end(JSON.stringify(resultado));
                        });
                    });

                } else if (request.url === '/users/me/avatar') {

                    let body = [];
                    request.on('data', (chunk) => {
                        body.push(chunk);
                    }).on('end', () => {
                        body = Buffer.concat(body).toString();
                        let datos = JSON.parse(body);

                        if (datos.avatar) {
                            let avatar = datos.avatar;
                            avatar = avatar.replace(/^data:image\/jpg;base64,/, "");
                            avatar = avatar.replace(/^data:image\/png;base64,/, "");
                            avatar = avatar.replace(/^data:image\/jpeg;base64,/, "");
                            avatar = Buffer.from(avatar, 'base64');

                            let date = new Date();
                            let nameFile = date.getDate() + date.getTime() + date.getMilliseconds() + '.jpg';
                            fs.writeFileSync('./img/users/' + nameFile, avatar);
                            datos.avatar = nameFile;
                        }

                        let modUser = new Usuario(datos);

                        modUser.actualizarAvatar(login.id, modUser).then(respuesta => {
                            token = AuthUser.generarToken(login);
                            resultado = {
                                error: false,
                                user: modUser,
                                token: token
                            };
                            response.end(JSON.stringify(resultado));

                        }).catch(error => {
                            resultado = {
                                error: true,
                                errorMessge: 'Error: ' + error
                            };
                            response.end(JSON.stringify(resultado));
                        });
                    });

                } else if (request.url === '/users/me/password') {

                    let body = [];
                    request.on('data', (chunk) => {
                        body.push(chunk);
                    }).on('end', () => {
                        body = Buffer.concat(body).toString();
                        let datos = JSON.parse(body);
                        let modUser = new Usuario(datos);

                        modUser.actualizarPassword(login.id, modUser).then(respuesta => {
                            token = AuthUser.generarToken(login);
                            resultado = {
                                error: false,
                                user: modUser,
                                token: token
                            };
                            response.end(JSON.stringify(resultado));

                        }).catch(error => {
                            resultado = {
                                error: true,
                                errorMessge: 'Error: ' + error
                            };
                            response.end(JSON.stringify(resultado));
                        });
                    });

                }

                // } else {
                //     //Si no se esta logueado se enviara un 403 a traves de la cabecera
                //     response.writeHead(
                //         403, {
                //             "Content-Type": "application/json"
                //         }
                //     );

                //     resultado = {
                //         error: true,
                //         errorMessge: 'User is not logged.'
                //     };

                //     response.end(JSON.stringify(resultado));
            }
            break;
        case 'DELETE':
            //3.3.4 Borrado de eventos-> probado funciona
            //Si existe el token
            if (valToken) {
                if (request.url.startsWith('/events/')) {
                    //Se parte la URL y se obtiene la id
                    let partes = request.url.split('/');
                    let id = partes[2];
                    Event.borrarEvento(id).then(respuesta => {
                        token = AuthUser.generarToken(login);
                        resultado = {
                            error: false,
                            ok: true,
                            result: "eventos borrados: " + respuesta,
                            token: token
                        };
                        response.end(JSON.stringify(resultado));

                    }).catch(error => {
                        resultado = {
                            ok: false,
                            error: true,
                            errorMessge: 'Error: ' + error
                        };
                        response.end(JSON.stringify(resultado));

                    })

                };

            } else {
                //Si no se esta logueado se enviara un 403 a traves de la cabecera
                response.writeHead(
                    403, {
                        "Content-Type": "application/json"
                    }
                );

                resultado = {
                    error: true,
                    errorMessge: 'User is not logged.'
                };

                response.end(JSON.stringify(resultado));
            }
            break;

        default:

            resultado = {
                error: true,
                errorMessge: "That request doesn't exists."
            };
            response.end(JSON.stringify(resultado));
            break;
    }
};

http.createServer(atenderPeticion).listen(8080);