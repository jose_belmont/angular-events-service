const conexion = require('./bd_config');
const md5 = require('md5');
const AuthUser = require('./authUser');
const jwt = require('jsonwebtoken');

const secreto = 'secret';


module.exports = class User {
    constructor(userJSON) {
        this.id = userJSON.id;
        this.name = userJSON.name;
        this.email = userJSON.email;
        this.password = userJSON.password;
        this.avatar = userJSON.avatar;
        this.lat = userJSON.lat;
        this.lng = userJSON.lng;
        this.mine = userJSON.mine;
    }

    //-------paquete GET -----------------//
    static buscarUsuarioLogueado(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT * FROM user WHERE user.id = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                user => {
                                    let us = new User(user);                                    
                                    us.mine = us.id === id ? true : false;
                                    return us;
                                })
                        );
                    }
                }
            );
        });
    }

    //buscar usuario por id -> funciona
    static buscarUsuarioPorId(id, login) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT * FROM user WHERE user.id = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            new User(resultado[0])
                        );
                    }
                }
            );
        });
    }

    //listar usuarios que acuden a un evento -> probar el fallo de colocacion en el index
    // el 'id' es del evento
    static listarUsuariosAsisten(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT * FROM user, user_attend_event WHERE user.id = user_attend_event.user AND ${id} = user_attend_event.event`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                user => new User(user)
                            )
                        );
                    }
                }
            );
        });
    }

    //-------paquete POST -----------------//

    //crear un usuario -> funciona
    crearUsuario() {
        return new Promise((resolve, reject) => {
            let data = {
                id: this.id,
                name: this.name,
                email: this.email,
                password: md5(this.password),
                avatar: this.avatar,
                lat: this.lat,
                lng: this.lng
            };

            conexion.query(
                'INSERT INTO user SET ?',
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );
        });
    }
    // si existe usuario de facebook o google
    static existeUsuario(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT * FROM user WHERE id_facebook = ${id} OR id_google = ${id}`, (error, resultado, campos) => {
                if (error) return reject(error);
                if (resultado[0]) {
                    resolve(new User(resultado[0]))
                }
                resolve(false);
            })
        })
    }

    //Crear un usuario por google
    static crearUsuarioGoogle(data) {
        let datos = {
            //id: this.id,
            id_google: data.id,
            email: data.emails[0].value,
            name: data.displayName,
            avatar: data.id + '.png'
        }

        return new Promise((resolve, reject) => {
            this.existeUsuario(datos.id_google).then(resultado => {
                if (!resultado) {
                    conexion.query('INSERT INTO user set ?', datos, (error, resultado, campos) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(
                            AuthUser.generarToken({
                                id: datos.id_google,
                                email: datos.email
                            })
                        );
                    });
                } else {
                    conexion.query({
                        sql: "UPDATE user SET ? WHERE email = ?",
                        values: [datos, datos.email]
                    }, (error, resultado, campos) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(
                            AuthUser.generarToken({
                                id: datos.id_google,
                                email: datos.email
                            })
                        );
                    });
                }
            });
        })
    }

    //Crear un usuario por facebook
    static crearUsuarioFacebook(data) {
        let datos = {
            //id: this.id,
            id_facebook: data.id,
            email: data.email,
            name: data.name,
            avatar: data.id + '.jpg'
        }
        return new Promise((resolve, reject) => {
            this.existeUsuario(datos.id_facebook).then(resultado => {
                if (!resultado) {
                    conexion.query('INSERT INTO user set ?', datos, (error, resultado, campos) => {

                        if (error) {
                            return reject(error => console.log(error));
                        }
                        resolve(

                            AuthUser.generarToken({
                                id: datos.id_facebook,
                                email: datos.email
                            })
                        );
                    });
                } else {
                    conexion.query({
                        sql: "UPDATE user SET ? WHERE email = ?",
                        values: [datos, datos.email]
                    }, (error, resultado, campos) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(
                            AuthUser.generarToken({
                                id: datos.id_facebook,
                                email: datos.email
                            })
                        );
                    });
                }
            });
        })
    }

    //-------paquete PUT -----------------//

    //actualizar nombre e email -> funciona
    actualizarEmail_nombre(id, user) {
        let data = {
            name: user.name,
            email: user.email
        }
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE user SET ? WHERE id = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    //actualizar avatar
    actualizarAvatar(id, user) {
        let data = {
            avatar: user.avatar
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE user SET ? WHERE id = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    //actualización del password
    actualizarPassword(id, user) {
        let data = {
            password: md5(user.password)
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE user SET ? WHERE id = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    //actualización coordenadas
    actualizarCoordenadas(id, lat, lng) {
        let data = {
            lat: lat,
            lng: lng
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE user SET ? WHERE id = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });

    }

    //actualización coordenadas
    actualizarCoordenadasFacebook(id, lat, lng) {
        let data = {
            lat: lat,
            lng: lng
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE user SET ? WHERE id_facebook = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });

    }

}