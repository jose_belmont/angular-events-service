const conexion = require('./bd_config');
const jwt = require('jsonwebtoken');
const md5 = require('md5');

const secret = 'secret';

module.exports = class AuthUser {
    static authUser(userJSON) {
        return new Promise((resolve, reject) => {
            conexion.query(
                'SELECT id, email FROM user WHERE email ="' + userJSON.email + '" AND password = "' + md5(userJSON.password) + '"',
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(
                            resultado.map(res => {
                                let login = {
                                    id: res.id,
                                    email: res.email
                                };
                                return this.generarToken(login);
                            })
                        );
                    }

                })
        });
    }

    static generarToken(login) {
        let token = jwt.sign(
            login, secret, {
                expiresIn: '600000 minutes'
            });
        return token;
    }

    static validarToken(token) {
        try {
            let resultado = jwt.verify(token, secret);
            return resultado;
        } catch (e) {
            return false;
        }
    }
}